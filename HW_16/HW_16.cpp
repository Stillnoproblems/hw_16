﻿#include <iostream>
#include <string>

using namespace std;

int main()
{   
   const short N = 5;
    

    int  Array[N][N];

    for (int i = 0; i < N; i++)
    {

        for (int j = 0; j < N; j++)
        {
            Array[i][j] = i + j;

        }
    }

    
    for (int i = 0; i < N; i++)
    {
        cout << "Line: " << i << "    ";

        for (int j = 0; j < N; j++)
        {
            cout << Array[i][j] << " ";
        }

        cout << endl;
    }
    
    cout << endl << endl;

    string Date;

    cout << "Input today's date in DD-MM-YYYY format: ";
    getline(cin, Date);
    cout << endl;

    short Day = 10 * (Date[0] - '0') + (Date[1] - '0');
    short SumInd = Day % N;
    int LineSumm = 0;

    for (int i = 0; i < N; i++)
    {
        if (i == SumInd)
        {
            for (int j = 0; j < N; j++)
            {
                LineSumm += Array[i][j];
            }
        }
    }

    cout << LineSumm;

  
    return 0;







}
